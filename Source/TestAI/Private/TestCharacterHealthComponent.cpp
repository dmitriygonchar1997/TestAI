// Fill out your copyright notice in the Description page of Project Settings.


#include "TestCharacterHealthComponent.h"
#include "TestCharacterBase.h"

DEFINE_LOG_CATEGORY_STATIC(TestCharacterHealthComponentLog, All,All);

// Sets default values for this component's properties
UTestCharacterHealthComponent::UTestCharacterHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

}

// Called when the game starts
void UTestCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CharacterOwner = Cast<ATestCharacterBase>(GetOwner());
	
	if(IsValid(CharacterOwner))
	{
		CharacterOwner->OnTakeAnyDamage.AddDynamic(this, &UTestCharacterHealthComponent::OnCharacterTakeDamage);
	}
}

void UTestCharacterHealthComponent::OnCharacterTakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Cast<ATestCharacterBase>(DamagedActor) && IsValid(CharacterOwner) && !bIsDead && Damage > 0)
	{
		if(!bGodMode)
		{
			HealthValue = FMath::Clamp(HealthValue - Damage, 0, MaxHealth);
		}

		UE_LOG(TestCharacterHealthComponentLog, Warning, TEXT("Character %s take damage. Health:%f, Damage amount:%f, Instigator:%s"), *CharacterOwner->GetName(),  HealthValue, Damage, *InstigatedBy->GetPawn()->GetName());

		OnCharacterDamaged.Broadcast(Damage, InstigatedBy);

		if (HealthValue <= 0)
		{
			bIsDead = true;

			UE_LOG(LogTemp, Warning, TEXT("Health deplated. Character Dead:%s"), *GetName());

			CharacterOwner->OnTakeAnyDamage.RemoveDynamic(this, &UTestCharacterHealthComponent::OnCharacterTakeDamage);

			OnCharacterDeath.Broadcast();

			CharacterOwner->SetLifeSpan(3.0f);
		}
	}
}

// Called every frame
void UTestCharacterHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

