// Fill out your copyright notice in the Description page of Project Settings.


#include "TestWeaponComponent.h"
#include "TestWeaponBase.h"
#include "TestCharacterBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UTestWeaponComponent::UTestWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UTestWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	SpawnWeapon();	
}

void UTestWeaponComponent::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (IsValid(CurrentWeapon))
	{
		CurrentWeapon->Destroy();
	}
}

void UTestWeaponComponent::SpawnWeapon()
{
	if(TObjectPtr<ATestCharacterBase> Character = Cast<ATestCharacterBase>(GetOwner()))
	{
		TSubclassOf<ATestWeaponBase> WeaponToSpawn;
		if (bRandomSpawnWeaponFromSet)
		{
			if (!WeaponsToSpawn.IsEmpty())
			{
				TArray<TSubclassOf<ATestWeaponBase>> Weapons = WeaponsToSpawn.Array();

				WeaponToSpawn = Weapons[UKismetMathLibrary::RandomIntegerInRange(0, Weapons.Num() -1)];
			}
		}
		
		else
		{
			WeaponToSpawn = ConcreateWeaponToSpawn;
		}

		CurrentWeapon = Character->GetWorld()->SpawnActor<ATestWeaponBase>(WeaponToSpawn, Character->GetMesh()->GetSocketTransform(!SocketToAttachWeapon.IsEmpty() ? *SocketToAttachWeapon : FName()));

		FAttachmentTransformRules TransformRules(EAttachmentRule::SnapToTarget, false);

		if (IsValid(CurrentWeapon))
		{
			CurrentWeapon->SetOwner(Character);

			CurrentWeapon->AttachToComponent(Character->GetMesh(), TransformRules, !SocketToAttachWeapon.IsEmpty() ? *SocketToAttachWeapon : FName());

			CurrentWeapon->SetShowWeaponTrace(bShowWeaponTrace);
			
			if (bUseRandomDamageForAllWeapons)
			{
				CurrentWeapon->SetUsingRandomDamage(true);
			}
		}
	}
}

void UTestWeaponComponent::Fire()
{
	if (IsValid(CurrentWeapon))
	{
		CurrentWeapon->Fire();
	}
}

void UTestWeaponComponent::SetUseRandomDamageForAllWeapons(bool bUse)
{
	if (IsValid(CurrentWeapon))
	{
		CurrentWeapon->SetUsingRandomDamage(bUse);
	}
}

// Called every frame
void UTestWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

