// Fill out your copyright notice in the Description page of Project Settings.


#include "TestWeaponBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "TestCharacterBase.h"
#include "TestWeaponComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "AIController.h"

DEFINE_LOG_CATEGORY_STATIC(WeaponLogCategory, All, All);


// Sets default values
ATestWeaponBase::ATestWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Mesh"));

	WeaponMesh->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void ATestWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

void ATestWeaponBase::Fire()
{
	UE_LOG(WeaponLogCategory, Warning, TEXT("Fire!"));

	FHitResult Hit;

	const FTransform MuzzleTransform = WeaponMesh->GetSocketTransform(TEXT("Muzzle"));
	const FVector StartTrace = MuzzleTransform.GetLocation();
	const FVector EndTrace = StartTrace + MuzzleTransform.GetRotation().GetForwardVector() * WeaponRange;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(GetOwner());

	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECollisionChannel::ECC_Pawn, Params);

	const TObjectPtr<ATestCharacterBase> HitCharacter = Cast<ATestCharacterBase>(Hit.GetActor());

	if(bShowWeaponTrace)
	{
		DrawDebugLine(GetWorld(), StartTrace, EndTrace, IsValid(HitCharacter) ? FColor::Red: FColor::Green, false, 5.0f, 0, 5.0f);
	}

	if (IsValid(HitCharacter))
	{
		const TObjectPtr<ATestCharacterBase> WeaponOwner = Cast<ATestCharacterBase>(GetOwner());
		if(IsValid(WeaponOwner))
		{
			const TObjectPtr<AAIController> AIOwner = Cast<AAIController>(WeaponOwner->GetController());
			const TObjectPtr<AAIController> AIHit = Cast<AAIController>(HitCharacter->GetController());

			if (IsValid(AIOwner) && IsValid(AIHit))
			{
				return;
			}

			const float Damage = bUseRandomDamage ? UKismetMathLibrary::RandomIntegerInRange(MinDamage, MaxDamage) : BaseDamage;

			UGameplayStatics::ApplyDamage(HitCharacter, Damage, WeaponOwner->GetController(), nullptr, UDamageType::StaticClass());
		}
	}
}

