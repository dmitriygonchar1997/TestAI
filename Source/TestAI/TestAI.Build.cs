// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TestAI : ModuleRules
{
	public TestAI(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "AIModule" });

		PublicIncludePaths.AddRange(new string[]
		{
				"TestAI/Public"
		});

        PrivateIncludePaths.AddRange(new string[]
{
                "TestAI/Private"
		});

    }
}
