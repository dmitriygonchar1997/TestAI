// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestAIGameMode.h"
#include "TestPlayerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "TestAISpawner.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"

ATestAIGameMode::ATestAIGameMode()
{
	DefaultPawnClass = ATestPlayerCharacter::StaticClass();
}

void ATestAIGameMode::SpawnRandomAI(int32 MaxSpawn, float TimeDelay)
{
	FTimerDelegate SpawnTimerDelegate;

	SpawnTimerDelegate.BindUObject(this, &ATestAIGameMode::SpawnRandomAICharacters, MaxSpawn);

	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, SpawnTimerDelegate, TimeDelay, false);
}

void ATestAIGameMode::SpawnRandomAICharacters(int32 MaxSpawn)
{
	const TObjectPtr<ATestAISpawner> SpawnerBox =  Cast<ATestAISpawner>(UGameplayStatics::GetActorOfClass(this, ATestAISpawner::StaticClass()));

	if (IsValid(SpawnerBox))
	{
		FVector Origin;
		FVector BoxExtent;
		SpawnerBox->GetActorBounds(false, Origin, BoxExtent);

		const TArray<TSubclassOf<ATestCharacterBase>> Spawners = CharacterAssetsToSpawn.Array();
		const int32 ToSpawnCount = UKismetMathLibrary::RandomIntegerInRange(1, MaxSpawn);

		int32 CurrentSpawn = 0;
		while (CurrentSpawn != ToSpawnCount)
		{
			FVector PointToSpawn = UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtent);
			
			GetWorld()->SpawnActor<ATestCharacterBase>(Spawners[UKismetMathLibrary::RandomIntegerInRange(0, Spawners.Num() - 1)], PointToSpawn, FRotator(0,0,0));
			++CurrentSpawn;
		}
	}
}