// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TestWeaponComponent.generated.h"


UCLASS(BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTAI_API UTestWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTestWeaponComponent();

protected:

	UPROPERTY(EditAnywhere, Category = "Settings|Spawn")
	TSet<TSubclassOf<class ATestWeaponBase>> WeaponsToSpawn;

	//Property that specifies a concrete weapon class to spawn when bRandomSpawnWeaponFromSet is set to false.
	UPROPERTY(EditAnywhere,  meta = (EDitCondition = "bRandomSpawnWeaponFromSet == false"),Category = "Settings|Spawn")
	TSubclassOf<ATestWeaponBase> ConcreateWeaponToSpawn;

	//Property that determines whether the weapon should be randomly chosen from the Weapons To Spawn set or if a concrete weapon class should be used.
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Random Spawn Weapon From Set"), Category = "Settings|Spawn")
	bool bRandomSpawnWeaponFromSet = false;

	UPROPERTY(EditAnywhere, Category = "Settings|Weapon")
	FString SocketToAttachWeapon;

	UPROPERTY(EditAnywhere, Category = "Settings|Weapon|Damage")
	bool bUseRandomDamageForAllWeapons = false;

	//Property that determines whether the weapon should show a trace visualization.
	UPROPERTY(EditAnywhere, Category = "Settings|Visualization")
	bool bShowWeaponTrace = true;

	TObjectPtr<ATestWeaponBase> CurrentWeapon;

	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

public:	

	/* 
		Responsible for spawning the weapon for the owner of the component.
		Determines which weapon class to spawn based on the bRandomSpawnWeaponFromSet flag.
		If bRandomSpawnWeaponFromSet is true, it randomly selects a weapon class from the WeaponsToSpawn array.
		If bRandomSpawnWeaponFromSet is false, it uses the ConcreateWeaponToSpawn class as the weapon class to spawn.
		It spawns the weapon using the SpawnActor function, passing in the determined weapon class and the owner's mesh socket transform as the spawn location.
		It sets the owner of the weapon using the SetOwner function, attaches the weapon to the owner's mesh using AttachToComponent, and sets the bShowWeaponTrace property of the weapon.
	*/
	UFUNCTION(BlueprintCallable)
	void SpawnWeapon();

	UFUNCTION(BlueprintCallable)
	void Fire();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void SetUseRandomDamageForAllWeapons(bool bUSse);

	UFUNCTION(BlueprintPure)
	FORCEINLINE ATestWeaponBase* GetCurrentWeapon() const { return CurrentWeapon; }
};
