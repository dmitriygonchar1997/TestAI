// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TestCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCharacterDamaged, float, DamageAmount, class AController*, InstigatedBy);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTAI_API UTestCharacterHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTestCharacterHealthComponent();

	UPROPERTY(BlueprintAssignable)
	FOnCharacterDeath OnCharacterDeath;

	UPROPERTY(BlueprintAssignable)
	FOnCharacterDamaged OnCharacterDamaged;

protected:

	UPROPERTY(EditAnywhere, Category = Health)
	float MaxHealth = 100.0f;

	//If this option is enabled, the character who owns this component will take damage, but his health will not be taken away.
	UPROPERTY(EditAnywhere, Category = Cheats)
	bool bGodMode = false;

	float HealthValue = MaxHealth;

	TObjectPtr<class ATestCharacterBase> CharacterOwner;

	bool bIsDead = false;
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnCharacterTakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetHealthValue() const { return HealthValue; }		
};
