// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TestCharacterBase.generated.h"

class UTestWeaponComponent;
class UTestCharacterHealthComponent;

UCLASS()
class TESTAI_API ATestCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATestCharacterBase();

protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TObjectPtr<UTestWeaponComponent> WeaponComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TObjectPtr<UTestCharacterHealthComponent> HealthComponent;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool bIsDeath = false;

public:	
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
