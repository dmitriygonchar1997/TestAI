// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestWeaponBase.generated.h"

UCLASS()
class TESTAI_API ATestWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:

	// Sets default values for this actor's properties
	ATestWeaponBase();

protected:

	UPROPERTY(EditAnywhere, Category = Components)
	TObjectPtr<USceneComponent> Root;

	UPROPERTY(EditAnywhere,Category = Components)
	TObjectPtr<class USkeletalMeshComponent> WeaponMesh;

	UPROPERTY(EditAnywhere, Category = "Settings|Range")
	float WeaponRange = 250.0f;

	//Used to set the minimum damage value when the bUseRandomDamage option is enabled.
	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseRandomDamage == true"), Category = "Settings|Damage")
	float MinDamage = 0.5f;

	//Used to set the maximum damage value when the bUseRandomDamage option is enabled.
	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseRandomDamage == true"), Category = "Settings|Damage")
	float MaxDamage = 100.0f;

	UPROPERTY(EditAnywhere,  Category = "Settings|Damage")
	float BaseDamage = 100.0f;

	//If this option is enabled, a damage value will be randomly selected for each new shot to deal damage with this weapon, in the range from MinDamage to MaxDamage.
	UPROPERTY(EditAnywhere,  Category = "Settings|Damage")
	bool bUseRandomDamage = false;

	bool bShowWeaponTrace = false;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	
	UFUNCTION(BlueprintCallable)
	FORCEINLINE void SetShowWeaponTrace(bool bIsShow) { bShowWeaponTrace = bIsShow; }

	virtual void Fire();

	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetWeaponRange() const { return WeaponRange; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE USkeletalMeshComponent* GetWeaponMesh() const { return  WeaponMesh; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE void SetUsingRandomDamage(bool bSet) { bUseRandomDamage = bSet; }
};
