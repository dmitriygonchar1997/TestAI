// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestAISpawner.generated.h"

UCLASS()
class TESTAI_API ATestAISpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestAISpawner();

	UPROPERTY(EditAnywhere, Category = "Components")
	TObjectPtr<class UBoxComponent> BoxComponent;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
