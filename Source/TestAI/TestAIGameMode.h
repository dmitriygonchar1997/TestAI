// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestAIGameMode.generated.h"

UCLASS(minimalapi)
class ATestAIGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestAIGameMode();

	//Spawn a random number of AI characters from 1 to MaxSpawn after a given period of time.
	UFUNCTION(Exec)
	void SpawnRandomAI(int32 MaxSpawn, float TimeDelay);

protected:

	FTimerHandle SpawnTimerHandle;

	UFUNCTION()
	void SpawnRandomAICharacters(int32 MaxSpawn);

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSet<TSubclassOf<class ATestCharacterBase>> CharacterAssetsToSpawn;
};



